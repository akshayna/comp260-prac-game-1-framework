﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{

	public float maxSpeed = 5.0f;
	// in metres per second
	public float acceleration = 7.0f;
	// in metres/second/second
	private float speed = 0.0f;
	// in metres/second
	public float brake = 5.0f;
	// in metres/second/second
	public float turnSpeed = 60.0f;
	// in degrees/second


	void Update ()
	{


		// the horizontal axis controls the turn
		float turn = Input.GetAxis ("Horizontal");

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis ("Vertical");

		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
			transform.Rotate (0, 0, -turn * turnSpeed * speed * Time.deltaTime);
		} else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
			transform.Rotate (0, 0, turn * turnSpeed * speed * Time.deltaTime);
		} else {
			// braking
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;

				if (Mathf.Abs (speed) < 0.3f && Mathf.Abs (speed) > -0.3f) {
					speed = 0.0f;
				}				


			} else {
				speed = speed + brake * Time.deltaTime;

				if (Mathf.Abs (speed) < 0.3f && Mathf.Abs (speed) > -0.3f) {
					speed = 0.0f;
				}
			}
		}

		// clamp the speed
		speed = Mathf.Clamp (speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate (velocity * Time.deltaTime, Space.Self);
	}

}